interface BaseButtonProps {
    children?: JSX.Element[] | JSX.Element
    classname?:string;
    label?:string;
    onClick?:React.MouseEventHandler<HTMLButtonElement>;
  }
  
  
  const BaseButton = ({children,classname,label,onClick}:BaseButtonProps)=> {
  
      return (
        <button className={`${classname} base-button  text-white font-bold py-1 px-3 rounded focus:outline-none focus:shadow-outline`} onClick={onClick}>
        {label}
        </button>
      )
    }
    
export default BaseButton