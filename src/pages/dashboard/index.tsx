import Dashboard from "@/layout/dashboard"


interface DashboardProps {
    children?: JSX.Element[] | JSX.Element
  
  }
  
  
const DashboardPage = ({children}:DashboardProps)=> {
  
      return (
       <Dashboard>
          <div>dashboard</div>
        </Dashboard>
      )
    }
    
export default DashboardPage